# README #

### What is this repository for? ###

This repository is the ros package for a fully autonomous guidance system
Version 0

### How to set up the ros_ws ###

Create a ros workspace (ros_ws) in the root folder of your ubuntu machine. The package uses ros kinect.
cd ~/ros_ws

Clone the repository in your existing workspace (ros_ws) under src folder.
Download and install the following libraries:

- map_server
- rtabmap_ros

Set up following libraries in the src folder of your workspace:

- iai_kinect2
- kinect2_bridge
- marvelmind_nav
- depthimage_to_laserscan

You should be able to access the packages using 

- cd ~/ros_ws/src/marvelmind_nav

Finally, in the workspace, use the following commands to set up the packages

- cd ~/ros_ws
` catkin_make 

To find ttyACM addresses for Arduinos connected through USB, use the command
- dmesg | grep tty
If needed, adjust the ttyACM number in tof_node and communication_node

### To start mapping facility ###
- roslaunch auto_guidance start_guidance.launch rtabmap_args:="--delete_db_on_start" rtabmapviz:=true localization:=false

### To save the map in pgm and yaml files, run the following command while mapping is still running ###
- rosrun map_server map_saver map:=rtabmap/grid_map

### To run the guidance once mapping is done ###
- roslaunch auto_guidance start_guidance.launch

### To plan waypoints manually ###
- cd ~/ros_ws/src/auto_guidance/resources
- rosrun map_server map_server map.yaml

In separate terminal (CNTRL + ALT + T):
- rosrun rviz rviz

(Add map to the display, you should see the map on rviz)
In separate terminal (CNTRL + ALT + T):
- rosrun auto_guidance waypoint

Use 2D Nav Goal button on rviz to plot waypoints and the copy the waypoints printed by the terminal 
and paste it in path_planner_node of auto_guidance package.



### Who do I talk to? ###

Autonomous team 37, Purdue University
Contact: jain131@purdue.edu
