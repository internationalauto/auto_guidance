#!/usr/bin/env python
# Author: Rodrigo, Dominik, Nishant, Christian
# path_planner_node.py

import rospy
import std_msgs
import nav_msgs
import math
import numpy
import tf

from auto_guidance.msg import Waypoint
from auto_guidance.msg import ShuttleStatus
from geometry_msgs.msg import Pose
from auto_guidance.msg import ObstacleSignal

waypoint_index = 0             #counter for the path points
loc_CallbackFlag = 0           #set flag to 0 to prevent path_planner executing without valid localization
xPos_car = -1                  #set x position to default value
yPos_car = -1                  #set y position to default value
pass_distance_waypoint = 0.35   #distance how close shuttle has to drive to waypoint before heading to next one

# b docking station is the test docking station 
# b (x,y,yaw degrees) = ( 1.73297631741 , -5.76310873032 , -90.9848390593 )
# This will be received from mission topic except yaw will be provided as a quaternion
# A docking station is the starting docking station
# A (x,y,yaw degrees) = ( 0.00560210179538 , -2.2806854248 , 0.997728002036 )

a_docking = ( 0.009775753133 , -0.20211699605 , -0.445868772917 )   # CENTRE OF THE DOCKING AREA A
b_docking = ( 0.0291423872113 , 5.41577911377 , -0.230867641141 )   # CENTRE OF THE DOCKING AREA B
c_docking = ( 2.68854045868 , 2.10350298882)     # CENTRE OF THE DOCKING AREA C




next_waypoint = (-1, -1, -1)   #x and y coordinate of new waypoint

#Example of one path
path=[( -0.325199604034 , -0.235100701451 , -28.5662995475 ),
( 0.732795774937 , -0.63552737236 , -1.77480400938 ),
( 1.29513478279 , 0.110492132604 , 65.2860366007 ),
( 1.82690799236 , 1.03576993942 , 52.1140924461 ),
( 2.00695204735 , 1.96563696861 , 29.6488505671 ),
( 2.68854045868 , 2.10350298882 , -0.359515318719 )]

#-----------------------------------------#
#----DEFINITIONS OF ALL POSSIBLE PATHS----#
#-----------------------------------------#

#path from station A pre-docking area to B pre-docking area, all paths are between pre-docking areas
AB=[( 0.0412450358272 , 0.377154064178 , 0.570916215803 ),
( -0.00575433066115 , 2.68853188 , 87.2490024116 ),
( 0.0165746808052 , 3.10165534019 , 89.1381107839 ),
( 0.0203904025257 , 3.97886323929 , 89.9032528226 ),
( 0.00906058773398 , 4.09786567688 , -0.635496272065 )]


#path from station A to C
AC=[( -0.325199604034 , -0.235100701451 , -28.5662995475 ),
( 0.732795774937 , -0.63552737236 , -1.77480400938 ),
( 1.29513478279 , 0.110492132604 , 65.2860366007 ),
( 1.82690799236 , 1.03576993942 , 52.1140924461 ),
( 2.00695204735 , 1.96563696861 , 29.6488505671 ),
( 2.68854045868 , 2.10350298882 , -0.359515318719 )]

#path from station B to A
BA=[( -0.325199604034 , -0.235100701451 , -28.5662995475 ),
( 0.732795774937 , -0.63552737236 , -1.77480400938 ),
( 1.29513478279 , 0.110492132604 , 65.2860366007 ),
( 1.82690799236 , 1.03576993942 , 52.1140924461 ),
( 2.00695204735 , 1.96563696861 , 29.6488505671 ),
( 2.68854045868 , 2.10350298882 , -0.359515318719 )]

#path from station B to C
BC=[( -0.325199604034 , -0.235100701451 , -28.5662995475 ),
( 0.732795774937 , -0.63552737236 , -1.77480400938 ),
( 1.29513478279 , 0.110492132604 , 65.2860366007 ),
( 1.82690799236 , 1.03576993942 , 52.1140924461 ),
( 2.00695204735 , 1.96563696861 , 29.6488505671 ),
( 2.68854045868 , 2.10350298882 , -0.359515318719 )]

#path from station C to A
CA=[( -0.325199604034 , -0.235100701451 , -28.5662995475 ),
( 0.732795774937 , -0.63552737236 , -1.77480400938 ),
( 1.29513478279 , 0.110492132604 , 65.2860366007 ),
( 1.82690799236 , 1.03576993942 , 52.1140924461 ),
( 2.00695204735 , 1.96563696861 , 29.6488505671 ),
( 2.68854045868 , 2.10350298882 , -0.359515318719 )]

#path from station C to B
CB=[( -0.325199604034 , -0.235100701451 , -28.5662995475 ),
( 0.732795774937 , -0.63552737236 , -1.77480400938 ),
( 1.29513478279 , 0.110492132604 , 65.2860366007 ),
( 1.82690799236 , 1.03576993942 , 52.1140924461 ),
( 2.00695204735 , 1.96563696861 , 29.6488505671 ),
( 2.68854045868 , 2.10350298882 , -0.359515318719 )]


#--------------------------------------------------------------------
# THE LOCALIZATION CALLBACK UPDATE THE POSITION VALUES OF THE SHUTTLE
# -------------------------------------------------------------------

# TODO global variables are supposed to be defined at the top of the file. The global keyword simply lets the 
# function know that it is supposed to use the global variable and not create its own new local variable
# initialize the missing global variables please

def localization_pos_callback(data):
  global loc_CallbackFlag     #flag for controller
  global xPos_car             #current x position from car on map
  global yPos_car             #current y position from car on map
  #global orientationPos      #current orientation from car on map
  
  xPos_car = data.position.x  #get current x position of car from localization_pos
  yPos_car = data.position.y  #get current y position of car from localization_pos
  
  loc_CallbackFlag = 1        #set flag to use path_planner

    
def shuttle_status_callback(data):
  global error_code_shuttle                     #error_codes from shuttle
  global charging_level_shuttle                 #actual battery level of shuttle
  
  error_code_shuttle = data.error_code          #get error_code_shuttle
  charging_level_shuttle  = data.charging_level #get battery level of shuttle


#------------------------------------------------------------------
# THE MISSION CALLBACK DEFINES WICH IS THE PATH THAT THE USER WANTS
#------------------------------------------------------------------

def mission_callback(data):
  #mission = (data.position.x, data.position.y, 0)
  global AB 
  global AC
  global BA 
  global BC
  global CA 
  global CB
  global path
  global waypoint_index
  global xPos_car
  global yPos_car
  global pass_distance_waypoint

  last_path = path       

  to_docking_A = math.sqrt((a_docking[0] - xPos_car)**2 + (a_docking[1] - yPos_car)**2)  # Distance of the shuttle to docking area A
  to_docking_B = math.sqrt((b_docking[0] - xPos_car)**2 + (b_docking[1] - yPos_car)**2)  # Distance of the shuttle to docking area B
  to_docking_C = math.sqrt((c_docking[0] - xPos_car)**2 + (c_docking[1] - yPos_car)**2)  # Distance of the shuttle to docking area C


  if(a_docking[0] == data.position.x & a_docking[1] == data.position.y):   # destiny is A?
    if(to_docking_B <= pass_distance_waypoint):                            # origin is B?
      path = BA
    elif(to_docking_C <= pass_distance_waypoint):                          # origin is C?
      path = CA
      
  elif(b_docking[0] == data.position.x & b_docking[1] == data.position.y): # destiny is B?
    if(to_docking_A <= pass_distance_waypoint):                            #origin is A?
      path = AB
    elif(to_docking_C <= pass_distance_waypoint):                          #origin is C?
      path = CB
  
  elif(c_docking[0] == data.position.x & c_docking[1] == data.position.y): # destiny is C?
    if(to_docking_A <= pass_distance_waypoint):                            # origin is A?
      path = AC
    elif(to_docking_B <= pass_distance_waypoint):                          # origin is B?
      path = BC    

  if(last_path != path):  #if there is a new path the waypoint_index is restarted
    waypoint_index = 0

#-------------------------------------------------
# THIS FUNCTION IMPLEMENTS THE PAHT PLANNING LOGIC
#-------------------------------------------------

def path_planner(next_waypoint_pub):
  global xPos_car                 #current x position from car on map
  global yPos_car                 #current y position from car on map
  global loc_CallbackFlag         #flag for valid localization
  global pass_distance_waypoint   #distance how close shuttle has to drive to waypoint before heading to next one
  global waypoint_index           #counter for the path points
  
  rate = rospy.Rate(100)           #set rate to 150Hz
  

  rospy.loginfo("Running path planner")

  # For testing and demonstration
  path = AB

  while not rospy.is_shutdown():
    if(loc_CallbackFlag == 1):
      curr_waypoint = path[waypoint_index]
      xdiff = curr_waypoint[0] - xPos_car     #calc the delta X between car position and way point position
      ydiff = curr_waypoint[1] - yPos_car     #calc the delta Y between car position and way point position
      hypo= math.sqrt(xdiff**2 + ydiff**2)    #calc the hypotenus. Distance from car to next distance
     

      if hypo < pass_distance_waypoint and waypoint_index < len(path):      #determine if the car is less than 0.2m from way point, stop the waypoint_index at the last waypoint in the path to prevent out of bounds later on and to signify "reached end of path"
          waypoint_index = waypoint_index+1   #determine when to update waypoint[] variable
          rospy.loginfo("Incrementing waypoint index")
      
      
      if waypoint_index == len(path):
        # reached the end
        waypoint = Waypoint()
        pos = Pose()
        pos.position.x = 0
        pos.position.y = 0
        waypoint.next_waypoint = pos
        waypoint.is_docking = 1   #the shuttle is in the pre-docking area
        rospy.loginfo("DOCKING STARTED")
        next_waypoint_pub.publish(waypoint)
      else:
        next_waypoint = path[waypoint_index]
        pos = Pose()
        pos.position.x = next_waypoint[0]
        pos.position.y = next_waypoint[1]
        # transform merged data into quaternion
        quaternion = tf.transformations.quaternion_from_euler(0, 0, next_waypoint[2])
        # fill up quaternions 
        pos.orientation.x = quaternion[0]
        pos.orientation.y = quaternion[1]
        pos.orientation.z = quaternion[2]
        pos.orientation.w = quaternion[3]
                         
        waypoint = Waypoint()

        # When shuttle is docking away from the docking station towards the pre-docking area
        # In that case the is_docking flag still needs to be 1 and the next_waypoint should be path[0]
        # When the shuttle reaches near the pre-docking area and you want ackermann motion now, reset the is_docking flag to 0
        if waypoint_index == 0 and hypo > pass_distance_waypoint:
          waypoint.is_docking = 1
          rospy.loginfo("DOCKING AWAY TO PRE-DOCKING AREA")
        else:
          waypoint.is_docking = 0
          rospy.loginfo("STARTING ACKERMANN TO NEXT MISSION")
        waypoint.next_waypoint = pos
        #Publish next waypoint
        next_waypoint_pub.publish(waypoint)
        #rospy.loginfo("Next waypoint: %f, %f", next_waypoint[0], next_waypoint[1])
  
    rate.sleep()


def initialize():
  
  rospy.init_node('path_planner_node')
  
  # Publishers for topics
  next_waypoint_pub = rospy.Publisher('next_waypoint', Waypoint, queue_size=10)
 
  # Subscribing to topics
  rospy.Subscriber('localization_pos', Pose, localization_pos_callback)
  rospy.Subscriber('shuttle_status', ShuttleStatus, shuttle_status_callback)
  rospy.Subscriber('mission', Pose, mission_callback)
  

  path_planner(next_waypoint_pub)
 
  rospy.spin() 


if __name__ == '__main__':
  initialize()
